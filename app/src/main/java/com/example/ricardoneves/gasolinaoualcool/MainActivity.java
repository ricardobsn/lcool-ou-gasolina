package com.example.ricardoneves.gasolinaoualcool;

import android.content.Intent;
import android.content.SharedPreferences;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String ARQUIVO = "arq_log";

    private TextView resultado;
    private Button verificar;
    private EditText precoAlcool;
    private EditText precoGasolina;
    private Button historico;
    private List<Info> historicoList = new ArrayList<>();
    private Info info;
    private String aux;
    float ultA= 0f;
    float ultG= 0f;

    private long lastUpdate = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lastUpdate = System.currentTimeMillis();
        saveLastUpdate();

        resultado= (TextView) findViewById(R.id.resultadoID);
        verificar= (Button) findViewById(R.id.verificarID);
        precoAlcool= (EditText) findViewById(R.id.alcoolID);
        precoGasolina= (EditText) findViewById(R.id.gasolinaID);
        historico= (Button) findViewById(R.id.historicoId);
        verificar.setEnabled(false);


        precoAlcool.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                verificarBotao();
            }
        });

        precoGasolina.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                verificarBotao();
            }
        });


        verificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String alcool = precoAlcool.getText().toString();
                String gasolina = precoGasolina.getText().toString();

                if(alcool == null || alcool.isEmpty() || gasolina == null || gasolina.isEmpty()){

                    Toast.makeText(getApplicationContext(),"Preencha os Campos Obrigatórios",
                            Toast.LENGTH_SHORT).show();
                }

                else {

                    try {
                        float gasolinaAux = Float.parseFloat(gasolina);
                        float alcoolAux = Float.parseFloat(alcool);
                        float resultadoFinal = gasolinaAux * 0.7f;

                        if (resultadoFinal == 0.0f) {
                            resultado.setText("Abasteça com Gasolina");
                        } else if (resultadoFinal > alcoolAux) {
                            resultado.setText("Abasteça com Álcool");
                        } else if (resultadoFinal < alcoolAux) {
                            resultado.setText("Abasteça com Gasolina");
                        }
                    } catch (NumberFormatException e) {
                        resultado.setText("Insira um Número Válido");
                    } catch (Exception e) {
                        resultado.setText("Ocorreu um erro inesperado.");
                    }
                }

                if (canUpdate(alcool, gasolina)) {
                    saveLastUpdate();
                    criarLog();
                } else {
                    Toast.makeText(getApplicationContext(),"Cálculo já Realizado!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        historico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.ARQUIVO, 0);
                JSONArray array = null;
                try {
                    String arraLog = sharedPreferences.getString("log", "[]");
                    long lastUpdate = sharedPreferences.getLong("last_update", 0);

                    array = new JSONArray(arraLog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if((array.length()==0)&&(historicoList.isEmpty())){
                    resultado.setText("Histórico Vazio");
                }


                startActivity(new Intent(MainActivity.this, SegundaActivity.class));
            }
        });
    }

    public boolean canUpdate(String a, String g) {
        float auxA= Float.parseFloat(a);
        float auxG= Float.parseFloat(g);


        if ((auxA!=ultA)||(auxG!=ultG)){
         ultA= auxA;
         ultG= auxG;

            return true;
        }
        else

            return false;

    }

    public void saveLastUpdate() {

        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("last_update", lastUpdate);
        editor.apply();
    }

    public void criarLog(){
        Date d1 = new Date();
        aux = resultado.getText().toString();
        info = new Info(d1, aux);
        historicoList.add(info);

        JSONArray jsonArray = new JSONArray();
        try {
            for (Info info : historicoList) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("data", info.getData().getTime());
                jsonObject.put("info", info.getInfo());

                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        String jsonObject = jsonArray.toString();

        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("log", jsonObject);
        editor.apply();
    }

    public void verificarBotao(){
        verificar.setEnabled(false);
        if( (precoAlcool.getText().length() > 0) &&(precoGasolina.getText().length()>0))
            verificar.setEnabled(true);
        }

    }





