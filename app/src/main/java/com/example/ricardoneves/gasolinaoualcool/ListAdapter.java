package com.example.ricardoneves.gasolinaoualcool;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class ListAdapter extends ArrayAdapter<Info> {

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapter(Context context, int resource, List<Info> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.itemlist, null);
        }

        Info p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.campoDataId);
            TextView tt2 = (TextView) v.findViewById(R.id.campoInfoId);

            if (tt1 != null) {
//                tt1.setText((CharSequence) p.getData().toString());
                tt1.setText((CharSequence) p.getPrettyData());
            }

            if (tt2 != null) {
                tt2.setText(p.getInfo());
            }
        }

        return v;
    }

}