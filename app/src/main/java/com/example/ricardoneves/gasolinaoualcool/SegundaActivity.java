package com.example.ricardoneves.gasolinaoualcool;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SegundaActivity extends Activity {

    private Button voltar;
    private ListView scroll;
    private ArrayList logDeCombustivel = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        voltar = (Button) findViewById(R.id.voltarId);
        scroll = (ListView) findViewById(R.id.listaId);


        Bundle extra = getIntent().getExtras();

        //logDeCombustivel = extra.getStringArrayList("log");

        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.ARQUIVO, 0);

        JSONArray array = null;

        try {

            String arraLog = sharedPreferences.getString("log", "[]");

            array = new JSONArray(arraLog);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<Info> inform = new ArrayList<Info>();

        Info inf = null;
        JSONObject obj;

        for (int i = 0; i < array.length(); ++i) {
            try {
                obj = array.getJSONObject(i);

                String dataString = obj.getString("data");

                long dataLong = Long.valueOf(dataString);

                String infoString = obj.getString("info");

                inf = new Info();

                inf.setData(new Date(dataLong));

                inf.setInfo(infoString);

            } catch (JSONException e) {
                e.printStackTrace();
            }


            inform.add(inf);
        }

        logDeCombustivel = (ArrayList) inform;

      /*  ArrayAdapter<ArrayList> adaptador = new ArrayAdapter<ArrayList>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                logDeCombustivel
        ); */
        ListAdapter customAdapter = new ListAdapter(this, R.layout.itemlist, inform);


        scroll.setAdapter(customAdapter);


        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SegundaActivity.this, MainActivity.class));

            }
        });


    }
}
